(function () {
    'use strict';

    angular.module('shop')
        /*
         * APIService
         *   APIService will use up to communicate with the remote server
         *   it will get the desired url parameters, build the url query and sent it to server
         *   it will return the result in form of $http that can be use with then,success or failure
         *   and will be bind to promise.
         * */
        .factory('APIService', function ( $http, $httpParamSerializerJQLike,$window) {
            var url = "";
            var apis = {
                'getAcsessToken' : 'https://login.salesforce.com/services/oauth2/token',
                'login' : 'https://eu11.salesforce.com/services/apexrest/login',
                'edit' : 'https://eu11.salesforce.com/services/apexrest/edit',
                'getProducts' : 'https://eu11.salesforce.com/services/apexrest/shop',
                'createOrder' : 'https://eu11.salesforce.com/services/apexrest/order',
                'register' : 'https://eu11.salesforce.com/services/apexrest/register'
                
            };
            var production_url = '';

            function GetAPIUrl(APIName) {
                var origin = production_url;
                return origin + apis[APIName];
            }
            return {
                getAcsessToken: function () {
                    url = GetAPIUrl("getAcsessToken");
                    return $http(
                        {
                            url: url,
                            method: "POST",
                            data : $httpParamSerializerJQLike({
                                'grant_type':'password',
                                'client_id' : '3MVG9HxRZv05HarQMWdBhAVE3XL9qtFrhHMsfhnuvzqwjD7TnTNigSlQouxCndKvmuhFoPrT8U5ScDsskdq7A',
                                'client_secret' : '8054190846148471690',
                                'username' : 'alexkor1987@test.com',
                                'password':'s6190612N6UYMoHPLCGE1rjFyERnD6CE'
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                               
                            }
                        });
                },
                login : function(username){
                    url = GetAPIUrl("login");
                    return $http(
                        {
                            url: url,
                            method: "POST",
                            data : {"username":username},
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token'],
                                 'Content-Type': 'application/json'
                            }
                        }); 
                },
                updateDetails : function(username,address){
                    url = GetAPIUrl("edit");
                    return $http(
                        {
                            url: url,
                            method: "PUT",
                            data : {"username":username,"address":address},
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token'],
                                 'Content-Type': 'application/json'
                            }
                        }); 
                },
                getShopProducts : function(){
                    url = GetAPIUrl("getProducts");
                    return $http(
                        {
                            url: url,
                            method: "GET",
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token']
                            }
                        }); 
                },
                createOrder : function(username, products, continueOrder){
                    url = GetAPIUrl("createOrder");
                    return $http(
                        {
                            url: url,
                            method: "POST",
                            data : {
                                'username':username,
                                'products':products,
                                'continueOrder':continueOrder
                            },
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token']
                            }
                        }); 
                },
                register : function(username, name, address){
                    url = GetAPIUrl("register");
                    return $http(
                        {
                            url: url,
                            method: "POST",
                            data : {
                                'username':username,
                                'name':name,
                                'address':address
                            },
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token']
                            }
                        }); 
                },
                getOrder : function(order){
                   
                    return $http(
                        {
                            url: 'https://eu11.salesforce.com/services/apexrest/order',
                            method: "GET",
                            params : {
                                'order':order
                            },
                            headers: {
                                'Authorization': 'Bearer '+ $window.localStorage['token'],
                                'Content-Type': 'application/json'
                            }
                        }); 
                },
                    
            };
                
        });
})();

