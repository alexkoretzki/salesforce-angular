angular.module('shop')
  .factory('TokenInterceptor', function ($q, $window, $location, $injector){
    return {
      request: function (config) {
        config.headers = config.headers || {};
        if($window.localStorage['token']) {
          config.headers.Authorization = 'Bearer ' +  $window.localStorage['token'];
        }
        return config;
      },
      requestError: function (rejection) {
        return $q.reject(rejection);
      },
      response: function(response) {
        return response || $q.when(response);
      },
      responseError: function (rejection) {
        if (rejection != null && rejection.status == 401) {
          if($window.localStorage['token'] != undefined){
            $window.localStorage.removeItem('token');
          }
          var stateService = $injector.get('$state');
          stateService.go('login',{}, { reload: true });
        }
        return $q.reject(rejection);
      }
    }
  });
