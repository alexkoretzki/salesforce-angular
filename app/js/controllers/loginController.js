(function () {
    'use strict';

    angular.module('shop')
        .controller('LoginController', ['$scope',  'APIService','$state',
            function($scope, APIService, $state) {
                    $scope.loginSuccess = true;
                    $scope.userData = {};

                    //login function, if success the user can edit his details or go to the shop
                    $scope.login = function(){
                        APIService.login($scope.userData.username).then(function successCallback(response) {
                            $scope.loginSuccess = false;
                            $scope.userData.address = response.data[0][0].adress__c; // show the user adress, updated view
                            
                        }, function errorCallback(response) {
                            $state.go('register');
                        });
                    }

                    $scope.edit = function(){
                        APIService.updateDetails($scope.userData.username, $scope.userData.address).then(function(res){
                                if(res.status == 204){
                                    $state.go('shop', {'user':$scope.userData.username});
                                }
                        });
                    }

                    $scope.goToShop = function(){
                         $state.go('shop', {'user':$scope.userData.username});
                    }
                               

            }]);
})();

