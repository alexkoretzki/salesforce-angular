    (function () {
        'use strict';

        angular.module('shop')
            .controller('ShopController', ['$scope',  'APIService','$state','getProductsFromShopAPI','$stateParams',
                function($scope, APIService, $state, getProductsFromShopAPI,$stateParams) {
                       $scope.myCart = []; 
                       $scope.products = getProductsFromShopAPI.data[0];
                       $scope.stockError = false;
                       $scope.user = $stateParams.user;
                  
                       
                       $scope.addToCart = function(product){
                            if($.inArray(product, $scope.myCart) != -1){
                                alert('you have this product in your cart alreay');
                                return;
                            }
                            $scope.myCart.push(product);
                       }

                      $scope.remove = function(index){
                        $scope.myCart.splice(index, 1);
                      }

                      $scope.createOrder = function(){
                        if (angular.isUndefinedOrNull($scope.user)){
                            alert('You have to login first');
                            $state.go('homepage');
                        }
                        else if($scope.myCart.length == 0){
                            alert('Your cart is empty');
                            return;
                        }
                        var order = createCart();
                        APIService.createOrder($scope.user,order,false).then(function successCallback(response) {
                               $state.go('confirmation',{'order':response.data});
                            }, function errorCallback(response) {
                               $scope.stockError = true;
                        });
                      
                      }

                      $scope.continueOrder = function(){
                        var order = createCart();
                        
                         APIService.createOrder($scope.user,order,true).then(function(res){
                            if(res.status == 201){
                                   $state.go('confirmation',{'order':res.data});
                            }
                       })
                      }

                      function createCart(){
                        var order = [];
                        for(var i =0 ;i<$scope.myCart.length;i++){
                            order.push($scope.myCart[i].Id);
                        }
                        return order;
                      }
                                   
                }]);
    })();

