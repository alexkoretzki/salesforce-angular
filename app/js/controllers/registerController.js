(function () {
    'use strict';

    angular.module('shop')
        .controller('RegisterController', ['$scope',  'APIService','$state',
            function($scope, APIService, $state) {
                    
                    $scope.userData = {};

                    $scope.register = function(){
                        APIService.register($scope.userData.username,$scope.userData.name,$scope.userData.address).then(function successCallback(response) {
                            $state.go('shop', {'user':$scope.userData.username});
                            
                        }, function errorCallback(response) {
                           alert('the user name is already in use, please choose different user name');
                           return;
                        });
                    }
            }]);
})();

