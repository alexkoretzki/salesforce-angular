(function () {
    'use strict';

    angular.module('shop')
        .controller('ConfirmationController', ['$scope',  'APIService','$state','getOrderDetails',
            function($scope, APIService, $state,getOrderDetails) {
                    $scope.order = getOrderDetails.data[0];
            }]);
})();

