'use strict';
// Declare app level module which depends on views, and components
angular.module('shop', [
    'ui.router'
   
]).
run(function($rootScope, APIService, $window) {
    APIService.getAcsessToken().then(function(res){
        $window.localStorage['token'] =  res.data.access_token;
    })

   
})
    .config(['$urlRouterProvider', '$stateProvider', '$httpProvider',
        function( $urlRouterProvider, $stateProvider, $httpProvider) {
            $stateProvider
                
                .state('homepage', {
                    url: "/",
                    controller: "LoginController as loginctrl",
                    templateUrl: "../partials/homepage.html"
                })
                 .state('register', {
                    url: "/register",
                    controller: "RegisterController as registerctrl",
                    templateUrl: "../partials/register.html"
                })
                 
                .state('shop', {
                    url: "/shop",
                    controller: "ShopController as shopctrl",
                    templateUrl: "../partials/shop.html",
                    params: {
                                user: null
                             } ,      
                    resolve : {
                        getProductsFromShopAPI: function(APIService){
                            return APIService.getShopProducts();
                        }
                    }
                })
                .state('confirmation', {
                    url: "/confirmation",
                    controller: "ConfirmationController as confirmationcontroller",
                    params: {
                                order: null
                            } , 
                    resolve : {
                       getOrderDetails: function(APIService,$stateParams){
                           return APIService.getOrder($stateParams.order);
                        }
                    },
                    templateUrl: "../partials/confirmation.html"
                });
               
               
            $urlRouterProvider.otherwise('/');
            angular.isUndefinedOrNull = function(val) {
                return angular.isUndefined(val) || val === null
            };
        }]);
